from tkinter import Frame, Label, LEFT, StringVar, Entry, RIGHT, TOP, X, Y, YES, CENTER


class Field:
    def __init__(self, master, field_name, label):
        self.field_name = field_name
        self.label = label
        self.row = Frame(master)
        self.row.pack(side=TOP, padx=10, pady=15, fill=X)
        self.label = Label(self.row, text=label, justify=LEFT)
        self.label.pack()
        s = StringVar()
        # s.set(field_name)

        self.entry = Entry(self.row, text=field_name, textvariable=s, justify=CENTER)
        self.entry.pack(side=RIGHT, expand=YES, fill=X, padx=10)
        if self.label["text"] == "Neptun code":
            self.entry.focus()

    def fetch(self):
        return self.field_name, self.entry.get()

    def clear(self):
        self.entry.delete(0, 'end')

    def focus(self):
        self.entry.focus()
