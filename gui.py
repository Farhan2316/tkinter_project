import json
from tkinter import Tk, Button, LEFT, messagebox

import yaml

from fields import Field


class GUI:

    def __init__(self, master):

        self.master = master
        master.title("My GUI Application")

        self.load_yaml()
        self.fields_class = []

        for field in self.fields:
            self.fields_class.append(Field(master, **field))

        self.array_to_save = []
        self.save_button = Button(master, text="Save", command=self.get_data, fg="blue", width=8)
        self.save_button.pack(padx=60, side=LEFT)
        self.exit_button = Button(master, text="Exit", command=self.insert_data, fg="red", width=8)
        self.exit_button.pack(side=LEFT)

    def load_yaml(self):
        with open(r"files/config.yaml") as f:
            try:
                data = yaml.safe_load(f)
                self.data_file = data["data_file"]
                self.fields = data["fields"]
            except yaml.YAMLError as exc:
                print(exc)
            except KeyError:
                print("Invalid configuration file!")

    def get_data(self):
        element = {}
        for i, field_class in enumerate(self.fields_class):

            if i == 0:
                field_class.focus()
            (field_name, label) = field_class.fetch()
            field_class.clear()
            element[field_name] = label

        if (element.get(field_name)) == '':
            messagebox.showerror("Error", "Please provide all Valid Inputs")

        else:
            if element.get("result") and element.get("mark"):
                try:
                    result = int(element.get("result"))
                    marks = int(element.get("mark"))
                    neptune_code = str(element.get("neptun_code"))
                    if 0 <= result <= 100 and 1 <= marks <= 5 and len(neptune_code) <= 10:
                        self.array_to_save.append(element)
                        print(self.array_to_save)
                    else:
                        messagebox.showerror("Error",
                                             "The value of neptun code, result and mark should be in the given Limit")

                except ValueError:
                    messagebox.showerror("Error", "The value of Result and mark should be Numbers")

    def insert_data(self):

        with open(self.data_file, 'w') as f:
            json.dump(self.array_to_save, f)
        self.master.quit()


def main():
    root = Tk()
    root.geometry("300x300+300+100")
    root.iconbitmap('img/image.ico')
    root.resizable(0, 0)
    my_gui = GUI(root)
    root.mainloop()
